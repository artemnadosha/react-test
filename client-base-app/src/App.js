import "./App.css";
import UserCard from "./components/UserCard/UserCard";
import UserList from "./components/UserList/UserList";
import UserOneInList from "./components/UserOneInList/UserOneInList";
import { useEffect, useState } from "react";
import { logDOM } from "@testing-library/react";

const API_URL = "https://jsonplaceholder.typicode.com/users";

function App() {
  const [data, setData] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [userInfo, setUserInfo] = useState(null);

  useEffect(() => {
    fetch(API_URL)
      .then((response) => response.json())
      .then((json) => {
        setData(json);
      })
      .catch((error) => console.log("Ошибка"));
  }, []);

  function openDetailsInfo(e) {
    setOpenModal(true);
    const infoTargetUser = data.filter(
      (user) => user.id === +e.currentTarget.id
    );
    setUserInfo(infoTargetUser[0]);
  }

  function closeDetailsInfo() {
    setOpenModal(false);
  }

  return (
    <div className="App">
      <h1>CLIENT BASE</h1>
      <UserList>
        {data.map((user) => (
          <UserOneInList
            onClick={openDetailsInfo}
            key={user.id}
            userId={user.id}
            name={user.name}
            email={user.email}
            city={user.address.city}
          />
        ))}
      </UserList>
      {openModal ? (
        <UserCard userInfo={userInfo} closeModal={closeDetailsInfo} />
      ) : (
        ""
      )}
    </div>
  );
}

export default App;
