import styles from './UserList.module.css'
const UserList = ({children}) => {
    return (
        <div className={styles.wrapper}>
            {children}
        </div>
    );
};

export default UserList;