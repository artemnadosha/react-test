import styles from "./UserOneInList.module.css";

const UserOneInList = ({ name, email, city, onClick, userId }) => {
  return (
    <div className={styles.wrapper} onClick={onClick} id={userId}>
      <div className={styles.userInfo}>
        <h2>{name}</h2>
        <p>{email}</p>
      </div>
      <p className={styles.userCity}>{city}</p>
    </div>
  );
};

export default UserOneInList;
