import styles from "./UserCard.module.css";
import { IoClose } from "react-icons/io5";

const UserCard = ({ userInfo, closeModal }) => {
  const { name, phone, email, website } = userInfo;
  const { city } = userInfo.address;

  return (
    <div className={styles.wrapper}>
      <div className={styles.modal}>
        <IoClose className={styles.iconClose} onClick={closeModal} />
        <div className={styles.userAvatar}>USER AVATAR</div>
        <div className={styles.detailInfoUser}>
          <h2 className={styles.fullName}>{name}</h2>
          <p>
            PHONE:<span>{phone.split(" ")[0]}</span>
          </p>
          <p>
            EMAIL:<span>{email}</span>
          </p>
          <p>
            CITY:<span>{city}</span>
          </p>
          <p>
            WEBSITE: <span>{website}</span>
          </p>
        </div>
      </div>
    </div>
  );
};

export default UserCard;
